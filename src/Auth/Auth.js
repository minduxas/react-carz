import auth0 from 'auth0-js';

class Auth {
  auth0 = new auth0.WebAuth({
    domain: 'minduxas.eu.auth0.com',
    clientID: 'N1B4YStHf3z1Y6PLYBvo4Ifjs556pTdn',
    redirectUri: 'http://localhost:3000/callback',
    responseType: 'token id_token',
    scope: 'openid',
  });

  login() {
    this.auth0.authorize();
  }

  logout() {
    this.auth0.logout();
  }
}

export default Auth;