import React, { Component } from 'react';

import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardLink,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';

import './CarList.css';

class CarList extends Component {
  render() {
    return (
      <div className="row">
        {this.props.cars.map((item, index) => (
          <div key={index} className="col-3">
            <Card>
              <CardImg
                top
                width="100%"
                src={item.Nuotrauka}
                alt="Card image cap"
              />
              <CardBody>
                <CardTitle>{item.Gamintojas}</CardTitle>
                <CardSubtitle>{item.Modelis}</CardSubtitle>
                <CardText>{item.Pavaru_dežė}</CardText>
              </CardBody>
            </Card>
          </div>
        ))}
      </div>
    );
  }
}

export default CarList;