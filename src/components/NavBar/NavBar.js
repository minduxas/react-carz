import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

import { Link } from 'react-router-dom';
import Auth from '../../Auth/Auth';

import './NavBar.css';

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {
    const auth = new Auth();

    return (
      <div>
        <Navbar color="dark" expand="md">
          <NavbarBrand href="/">CARZ mašinų nuoma</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <Link to="/cars/" className="nav-link">
                  Mašinos
                </Link>
              </NavItem>
              <NavItem>
                <Link to="/rentalpoints/" className="nav-link">
                  Nuomos punktai
                </Link>
              </NavItem>
              <NavItem>
                <Link to="/contacts/" className="nav-link">
                  Kontaktai
                </Link>

                <NavItem>
                  <NavLink onClick={() => auth.login()}>Login</NavLink>
                </NavItem>

                <NavItem>
                  <NavLink onClick={() => auth.logout()}>Logout</NavLink>
                </NavItem>

              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>Option 1</DropdownItem>
                  <DropdownItem>Option 2</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Reset</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
