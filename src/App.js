import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import './App.css';

import NavBar from './components/NavBar';
import Cars from './containers/Cars';
import RentalPoints from './containers/RentalPoints';
import Contacts from './containers/Contacts';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <NavBar />

          <div className="container">
            <Route exact path="/" component={Cars} />
            <Route path="/cars" component={Cars} />
            <Route path="/cars/:id" component={Cars} />
            <Route path="/rentalpoints" component={RentalPoints} />
            <Route path="/contacts" component={Contacts} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
