import React, { Component } from 'react';
import axios from 'axios';

// import { Button, Form, FormGroup, Label, Input, Alert, Col } from 'reactstrap';

import './Cars.css';

import CarList from '../../components/CarList/CarList';

class Cars extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cars: [],
    };
  }

  componentDidMount() {
    this.fetchCars();
  }

  fetchCars() {
    axios
      .get('http://localhost:3000/cars')
      .then(cars => {
        this.setState(state => ({
          cars: cars.data,
          error: undefined,
        }));
      })
      .catch(error => {
        this.setState(state => ({
          cars: state.cars,
          error: error.message,
        }));
      });
  }

  render() {
    return (
      <div>
        <h1>Mašinos</h1>
        <CarList cars={this.state.cars} />
        <footer>footer</footer>
      </div>
    );
  }
}

export default Cars;