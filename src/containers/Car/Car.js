import React, { Component } from 'react';
import axios from 'axios';

import CarList from '../../components/CarList/CarList';

class Car extends Component {
  constructor(props) {
    super(props);

    this.state = {
      car: {},
    };
  }

  componentDidMount() {
    this.fetchCar();
  }

  fetchCar(carID) {
    axios
      .get(`http://localhost:3000/cars/${carID}`)
      .then(car => {
        this.setState(state => ({
          car: car,
          error: undefined,
        }));
      })
      .catch(error => {
        this.setState(state => ({
          car: state.car,
          error: error.message,
        }));
      });
  }

  render() {
    return (
      <div>
        <h1>Audi Q5</h1>
        <CarList car={this.state.car.id} />
      </div>
    );
  }
}

export default Car;